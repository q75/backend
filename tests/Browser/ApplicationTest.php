<?php

namespace Tests\Browser;

use App\Models\User;
use Tests\DuskTestCase;

class ApplicationTest extends DuskTestCase
{

    public function setUp(): void
    {
        parent::setUp();
    }

    public function getAccessToken()
    {
        $user = User::find(1);
        $response = $this->json('POST', '/api/v1/login',
            ['email' => $user->email, 'password' => '123456']);
        return "Bearer " . $response['data']['accessToken'];
    }

    public function failLoginData()
    {
        return [
            ['ali.zahedi1371@gmail.com', '123456', 422, "The selected email is invalid."],
            ['', '123456', 422, 'The email field is required.'],
            ['ali.zahedi1371@gmail.com', '', 422, 'The password field is required.']
        ];
    }

    /**
     * Test fail login test.
     * @dataProvider failLoginData
     * @return void
     */
    public function testFailLogin($email, $password, $status, $message)
    {
        $response = $this->json('POST', '/api/v1/login',
            ['email' => $email, 'password' => $password]);
        $response->assertStatus($status)->assertSee($message);
    }

    public function successLoginData()
    {
        return [
            ['123456', 200, 'data', 'accessToken']
        ];
    }

    /**
     * Test success login test.
     * @dataProvider successLoginData
     * @return void
     */
    public function testSuccessLogin($password, $status, $tokenKeyName)
    {
        $user = User::where('id', rand(1, 10))->first();
        $response = $this->json('POST', '/api/v1/login',
            ['email' => $user->email, 'password' => $password]);
        $response->assertStatus($status)->assertSee($tokenKeyName);
    }

    public function searchCompaniesData()
    {
        return [
            [401, 'a', "You are unauthenticated."],
            [422, '', "The search word field is required."],
            [200, 'a', "name"]
        ];
    }

    /**
     * Test success login test.
     * @dataProvider searchCompaniesData
     * @return void
     */
    public function testCompaniesSearch($status, $searchWord, $message)
    {
        $token = ($status != 401) ? $this->getAccessToken() : '';
        $response = $this->json('GET', '/api/v1/company/search',
            ['search_word' => $searchWord], ['Authorization' => $token]);
        $response->assertStatus($status)->assertSee($message);
    }
}
