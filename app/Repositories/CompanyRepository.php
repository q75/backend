<?php

namespace App\Repositories;

use App\Models\Company;
use App\Repositories\Interfaces\CompanyRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class CompanyRepository extends Repository implements CompanyRepositoryInterface
{
    protected $model;

    public function __construct(Company $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    public function searchByName(string $name, array $selectedColumns = ['*']): Collection
    {
        return $this->model->where('name', 'like', '%' . $name . '%')->select($selectedColumns)->get();
    }
}
