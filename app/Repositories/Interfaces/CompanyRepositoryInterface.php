<?php

namespace App\Repositories\Interfaces;

interface CompanyRepositoryInterface extends RepositoryInterface
{
    public function searchByName(string $name, array $selectedColumns = ['*']);
}
