<?php

namespace App\Exceptions;

use App\Services\ResponseService;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Container\Container;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Throwable;

class Handler extends ExceptionHandler
{

    private $responseService;

    public function __construct(Container $container, ResponseService $responseService)
    {
        parent::__construct($container);
        $this->responseService = $responseService;
    }

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $e)
    {
        if ($e instanceof ValidationException && $request->is('api/*')) {
            return $this->responseService->respond($e->errors(), 422);
        } elseif ($e instanceof AuthenticationException && $request->is('api/*')) {
            return $this->responseService->respond(["message" => "You are unauthenticated."], 401);
        } elseif ($e instanceof MethodNotAllowedHttpException && $request->is('api/*')) {
            return $this->responseService->respond(["message" => "Your request is invalid."], 405);
        } elseif ($e instanceof \Exception && $request->is('api/*')) {
            return $this->responseService->respond(["message" => $e->getMessage()], 422);
        }
    }
}
