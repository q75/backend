<?php

namespace App\Http\Controllers\V1;

use App\Http\Requests\V1\Auth\LoginRequest;
use App\Services\AuthService;
use App\Services\ResponseService;
use Illuminate\Http\JsonResponse;

class AuthController
{
    protected $authService;
    private $responseService;

    public function __construct(AuthService $authService,
                                ResponseService $responseService)
    {
        $this->authService = $authService;
        $this->responseService = $responseService;
    }

    public function login(LoginRequest $loginRequest): JsonResponse
    {
        $loginResults = $this->authService->loginService($loginRequest->input('email'), $loginRequest->input('password'));
        return $this->responseService->respond($loginResults['data'], $loginResults['status']);
    }
}
