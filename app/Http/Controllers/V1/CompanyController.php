<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\V1\Company\SearchRequest;
use App\Services\CompanyService;
use App\Services\ResponseService;
use Illuminate\Http\JsonResponse;

class CompanyController extends Controller
{
    private $companyService;
    private $responseService;

    public function __construct(ResponseService $responseService,
                                CompanyService $companyService)
    {
        $this->companyService = $companyService;
        $this->responseService = $responseService;
    }

    public function search(SearchRequest $searchRequest): JsonResponse
    {
        return $this->responseService->respond($this->companyService->search($searchRequest->input('search_word')));
    }
}
