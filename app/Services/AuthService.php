<?php

namespace App\Services;

use App\Repositories\Interfaces\UserRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class AuthService
{

    private $responseService;
    private $userRepository;

    public function __construct(ResponseService $responseService,
                                UserRepositoryInterface $userRepository)
    {
        $this->responseService = $responseService;
        $this->userRepository = $userRepository;
    }

    public function loginService(string $email, string $password): array
    {
        $user = $this->userRepository->findByEmail($email);
        if ($user->count() > 0) {
            if (Hash::check($password, $user->password)) {
                $token = $user->createToken($email . ' : ' . Carbon::now())->accessToken;
                return ["data" => ["accessToken" => $token, "user" => $user], "status" => 200];
            } else {
                return ["data" => ["message" => "Your password is wrong."], "status" => 422];
            }
        }
    }
}
