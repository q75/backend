<?php

namespace App\Services;

use App\Repositories\Interfaces\CompanyRepositoryInterface;

class CompanyService
{
    private $companyRepository;
    private $curlService;

    public function __construct(CompanyRepositoryInterface $companyRepository,
                                CurlService $curlService)
    {
        $this->companyRepository = $companyRepository;
        $this->curlService = $curlService;
    }

    public function search(string $searchWord): array
    {
        $results = $this->curlService->get(env('COMPANY_URL') . '/search', ['q' => $searchWord]);
        return $this->makeResponses($results);
    }

    private function makeResponses(string $results): array
    {
        $decodedData = json_decode($results);
        return (isset($decodedData->data->items)) ? $decodedData->data->items : [];
    }
}
