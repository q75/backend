<?php

namespace App\Services;

use Ixudra\Curl\Facades\Curl;

class CurlService
{
    public function get(string $url, array $params = [], array $headers = []): string
    {
        return Curl::to($url)
            ->withHeaders($headers)
            ->withData($params)
            ->get();
    }
}
